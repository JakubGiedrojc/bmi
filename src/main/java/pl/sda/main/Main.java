import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Podaj wagę");
        Scanner scanner = new Scanner(System.in);
        double waga = scanner.nextDouble();
        System.out.println("podaj wzrost w metrach");
        double wzrost = scanner.nextDouble();

        double resultBMI = BMI.bmiCalc(waga, wzrost);

        if (resultBMI<18.4 ||resultBMI>24.9){
            System.out.println("BMI nieprawidłowe");
        }
        else
            System.out.println("BMI prawidłowe");

    }

}
